<?php

namespace App\Http\Controllers\Admin\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\Feecategories;

class FeeCategoryController extends Controller
{
    public function index()
    {
        $data = Feecategories::all();
        return view('backend.student-management.fee-category.view',compact('data'));
    }

   
    public function create()
    {
        return view('backend.student-management.fee-category.create');
    }

    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'fee_category' => 'required|unique:feecategories|max:25',
        ]);
        $data = $request->all();
        Feecategories::create($data);
        return back()->with('success','fee-category Added Successfully');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $data = Feecategories::find($id);
        return view('backend.student-management.fee-category.edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'fee_category' => 'required|unique:fstudent-management.eecategories|max:25',
        ]);
        $data = Feecategories::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return back()->with('success','fee-category Update Successfully');
    }

    
    public function destroy($id)
    {
        $data = Feecategories::find($id);
        $data->delete();
        return back()->with('success','fee-category Deleted Successfully');
    }
}
