<?php

namespace App\Http\Controllers\Admin\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\Shifts;

class ShiftController extends Controller
{
    public function index()
    {
        $data = Shifts::all();
        return view('backend.student-management.shift.view',compact('data'));
    }

   
    public function create()
    {
        return view('backend.student-management.shift.create');
    }

    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'shift_name' => 'required|unique:shifts|max:25',
        ]);
        $data = $request->all();
        Shifts::create($data);
        return back()->with('success','shift Added Successfully');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $data = Shifts::find($id);
        return view('backend.student-management.shift.edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'shift_name' => 'required|unique:shifts|max:25',
        ]);
        $data = Shifts::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return back()->with('success','shift Update Successfully');
    }

    
    public function destroy($id)
    {
        $data = Shifts::find($id);
        $data->delete();
        return back()->with('success','shift Deleted Successfully');
    }
}
