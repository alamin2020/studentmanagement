<?php

namespace App\Http\Controllers\Admin\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Department;
use App\Admin\Subjects;

class SubjectController extends Controller
{
    
    public function index()
    {
        // $data = Subjects::all();
        $data = Subjects::join('departments','Subjects.departments_id','=','departments.id')->select('Subjects.*','departments.dpt_name')->get();
        return view('backend.student-management.subject.view',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::all();
        return view('backend.student-management.subject.create',compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'subject_name' => 'required',
            'departments_id' => 'required',
        ]);
        $data = $request->all();
        Subjects::create($data);
        return back()->with('success','Subject Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Subjects::find($id);
        $department = Department::all();
        return view('backend.student-management.subject.edit',compact('data','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'subject_name' => 'required',
            'departments_id' => 'required',
        ]);
        $data = Subjects::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return back()->with('success','Subject Data update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Subjects::find($id);
        $data->delete();
        return back()->with('success','Subject Data Has been Delete Successfully');
    }
}
