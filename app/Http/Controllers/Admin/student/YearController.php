<?php

namespace App\Http\Controllers\Admin\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\Years;

class YearController extends Controller
{
    public function index()
    {
        $data = Years::all();
        return view('backend.student-management.year.view',compact('data'));
    }

   
    public function create()
    {
        return view('backend.student-management.year.create');
    }

    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'year' => 'required|unique:years|max:25',
        ]);
        $data = $request->all();
        Years::create($data);
        return back()->with('success','Year Added Successfully');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $data = Years::find($id);
        return view('backend.student-management.year.edit',compact('data'));
    }

    
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'year' => 'required|unique:years|max:25',
        ]);
        $data = Years::find($id);
        $new_data = $request->all();
        $data->update($new_data);
        return back()->with('success','Year Update Successfully');
    }

    
    public function destroy($id)
    {
        $data = Years::find($id);
        $data->delete();
        return back()->with('success','Year Deleted Successfully');
    }
}
